# Stack
# Stack is a linear data structure that follows the Last In / First Out (LIFO) or First In / Last Out (FILO) principles

stack = []

# Function to check if a stack is empty through len() function
def empty(stack):
	# len() function is used to check if the stack is empty
	if len(stack) == 0:
		print("The stack is empty.")
	else:
		# print(f'The stack are: {stack}')
		print("The stack are {items}".format(items = stack))

empty(stack)
# result: The stack is empty
stack.append(5)
stack.append(6)
stack.append(10)
print(stack) # result: [5, 6, 10]
print(len(stack)) # result: 3

# Function to check the last element of the stack
def top(stack):
	if len(stack) == 0:
		print('Stack the empty.')
	else:
		print(stack[-1])

top(stack) # result: 10

stack.append(11)
print(stack) # result: [5, 6, 10, 11]

# Since stack is LAST IN FIRST OUT (LIFO) the function to remove values should remove from the end, a perfection for this is pop()
stack.pop()
print(stack) # result: [5, 6, 10]

# Deque / DEQue
# Double Ended Queue
print("----------------------")
from collections import deque
# We use the deque function so we could also access function specialized for deque such as extendleft(), additionally rotate()
stack_friends = deque(["Rachel", "Ross", "Joey", "Monica", "Chandler"])
print(stack_friends)

empty(stack_friends)

print(len(stack_friends))

top(stack_friends)

# Deque means Double Ended Queue
# Adds or remove elements in the head, tail, or even in the middle
# A deque is a data structure that allows insertion and deletion at both ends making it more versatile than regular queue or stack.

# Adding and removing from the tail
stack_friends.append("Phoebe")
print(stack_friends)
stack_friends.pop()
print(stack_friends)

# Finding index of "Joey"

print(stack_friends.index("Joey", 0, 4))
print(stack_friends.index("Joey", 1, 3))

stack_friends.append("Rachel")
print(stack_friends.count("Rachel"))
print(stack_friends)

stack_friends.insert(4, "Phoebe")
print(stack_friends)

stack_friends.remove("Rachel")
print(stack_friends)

# Add in the head of the stack
stack_friends.extendleft(["Vedant"])
print(stack_friends)

# Using extend left, the last item in the list that we want to add will be the first item of the list (stack_friends)
stack_friends.extendleft(["Sheldon", "Leonard", "Topher"])
print(stack_friends)

# remove("element")
stack_friends.remove("Joey")
print(stack_friends)

# queue
# Queue is a linear data structure that stores items in First in First Out (FIFO) manner
print("-------------------------------")

import queue

sample_students = queue.Queue(maxsize = 5)
# sample_students = queue.LifoQueue(maxsize = 5)

# empty() in queue returns a Boolean (True or False)
print(sample_students.empty())

# put() adds item in the queue
# Unlike stack or regular list that we use append(), here in queue we use put() function
sample_students.put("Rick")
sample_students.put("Morty")
sample_students.put("Summer")
sample_students.put("Jerry")
sample_students.put("Beth")
print(sample_students.queue)

# qsize() - returns total number of items
print(sample_students.qsize())
# print(len(sample_students.queue))... it works too

# get() also like pop() it removes an element, if it is a Queue() it will remove the first item, if it is a LifoQueue() it will remove the last item
# get() also returns the removed item
print(sample_students.get())
print(sample_students.queue)

# full() checks if it occupates to the full / max size
print(sample_students.full())

# qsize() - returns total number of items
print(sample_students.qsize())

# maxsize
# retrieve the declared maximum size
print(sample_students.maxsize)